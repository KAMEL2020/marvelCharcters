from django.urls import path
from . import views

urlpatterns = [

    path('characters/', views.getHeros, name="characters"),
    path('charNames/', views.getNames, name='characterNames'),
    path('charStories/', views.getStories, name="storeis"),
    path('charComics/', views.getComics, name="comics"),
    path('charSeries/', views.getSeries, name="series"),
    path('charPicture/', views.getCharPic, name="pictures"),

]
