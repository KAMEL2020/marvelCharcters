from urllib import response
from django.shortcuts import render
from marvel import Marvel
from rest_framework.decorators import api_view
from rest_framework.response import Response
import requests
import json
# Create your views here.
Call_API = requests.get(
    "http://gateway.marvel.com/v1/public/characters?ts=1&apikey=19ad249e4deeb789e01b1a514af5000e&hash=c03238d1497b78f5da4edf0ac76b7c0b")
allData = Call_API.text
result = json.loads(allData)


@api_view(['GET'])
def getHeros(request):
    response_API = requests.get(
        "http://gateway.marvel.com/v1/public/characters?ts=1&apikey=19ad249e4deeb789e01b1a514af5000e&hash=c03238d1497b78f5da4edf0ac76b7c0b")
    data = response_API.text
    responses = json.loads(data)
    # c03238d1497b78f5da4edf0ac76b7c0b hash
    return Response(responses)

# function getNames filters the names of the characters APIs


@api_view(['GET'])
def getNames(request):
    nameList = []
    for i in result["data"]["results"]:
        res = i
        included_keys = ['id', 'name']
        d = {k: v for k, v in res.items() if k in included_keys}
        nameList.append(d)

    return Response(nameList)


# function getNames filters the stories of the characters APIs
@api_view(['GET'])
def getStories(request):
    nameStoriesList = []
    for i in result["data"]["results"]:
        res = i["stories"]["items"]
        for j in res:
            included_keys = ['name']
            d = {k: v for k, v in j.items() if k in included_keys}
            temp = {}
            temp["id"] = i["id"]
            d.update(temp)
            nameStoriesList.append(d)
    return Response(nameStoriesList)


@api_view(['GET'])
def getComics(request):
    nameComicList = []
    for i in result["data"]["results"]:
        res = i["comics"]["items"]
        for j in res:
            included_keys = ['name']
            d = {k: v for k, v in j.items() if k in included_keys}
            temp = {}
            temp["id"] = i["id"]
            d.update(temp)
            nameComicList.append(d)
    return Response(nameComicList)


@api_view(['GET'])
def getSeries(request):
    nameSeriesList = []
    for i in result["data"]["results"]:
        res = i["series"]["items"]
        for j in res:
            included_keys = ['name']
            d = {k: v for k, v in j.items() if k in included_keys}
            temp = {}
            temp["id"] = i["id"]
            d.update(temp)

            nameSeriesList.append(d)
    return Response(nameSeriesList)


# function getNames filters the Photos of the characters APIs

@api_view(['GET'])
def getCharPic(request):
    pictures = []
    for i in result["data"]["results"]:
        res = i
        included_keys = ['id', 'thumbnail']
        d = {k: v for k, v in res.items() if k in included_keys}
        pictures.append(d)

    return Response(pictures)
